#!/bin/bash

# Tested OK with knoppix v7.0.4, v6.7.1
# For v6.2.1, the parameters for a netcat server is "nc -l -p port", so needs minor modification to work
# ...but now using busybox; have not tested on v6.2.1 yet.

kiso="$1" # path to knoppix ISO

if [ "$2" = "" ]
then
    targetdir="$(pwd)/scriptable-knoppix-setup"
else
    targetdir="${2%/}"
fi


set -e  # stop on any error

dofail()
{
    echo "Failed...exiting. ($@)"
    exit 255
}

[ -f "$kiso" ] || dofail "Not found: $kiso"
[ "$(whoami)" = "root" ] || dofail "Must run as root"
[ -d "$targetdir" ] && dofail "$targetdir already exists"
[ -f make-scriptable-kvm-knoppix.sh ] && [ -f busybox ] || dofail "must run from same dir as this script and busybox"

bbpath="$(pwd)"

mkdir "$targetdir"

# (1) extract linux, conf, and minirt.gz

mkdir "$targetdir/miso"

mount "$kiso" "$targetdir/miso" -o loop

cp "$targetdir/miso/boot/isolinux/linux" "$targetdir"
cp "$targetdir/miso/boot/isolinux/linux64" "$targetdir"
cp "$targetdir/miso/boot/isolinux/isolinux.cfg" "$targetdir"
cp "$targetdir/miso/boot/isolinux/minirt.gz" "$targetdir"

umount "$targetdir/miso" || { sleep 3 && umount "$targetdir/miso" ; }
rmdir "$targetdir/miso"

# (2) expand minirt

mkdir "$targetdir/expanded"

(
    cd "$targetdir/expanded"
    cat ../minirt.gz | gunzip -c |  cpio -imd --no-absolute-filenames

)

# (3) modify init

(
    cd "$targetdir/expanded"

    ls -l init
    execLine="$(grep -n '^exec ' init | tail -n 1)"
    execLine="${execLine%%:*}"
    totLines="$(cat init | wc -l)"
    
    cp -a init init.org
    
    # FIRST PART
    head -n $(( execLine - 2 )) init.org  >init
    ls -l init

    # NEW PART
    cat >>init <<'EOF'
## extra lines for minirt/init start

cat >/home/knoppix/simple-guest-bash-server.sh <<'EOF3'
#!/bin/bash

prefixitin()
{
   prefix="$1"
   IFS=""
   while read -r ln 
   do
     echo "$prefix$ln" 1>"$xtermtty"  # send to xterm in KVM
     [ "$ln" == "xxEOFxx" ] && exit
     echo "$ln"
   done
}

prefixitout()
{
   prefix="$1"
   IFS=""
   while read -r ln 
   do
     echo "$prefix$ln" 1>"$xtermtty"  # send to xterm in KVM
     echo "$ln" >&99 # send to host
   done
}

prefixiterr()
{
   prefix="$1"
   IFS=""
   while read -r ln 
   do
     echo "$prefix$ln" 1>"$xtermtty"  # send to xterm in KVM
     echo "$prefix$ln" >&99 # send to host
   done
}

closefds()
{
   # needed because for the redirection of stdout and stderr, the
   # original file descriptors are created at e.g. 62 and 63 and
   # copied to 1 and 2.  Created background processes will get these
   # too, and then bash will refuse to exit with its stderr and stdout
   # still open in other processes.
   for (( i=3 ; i<100 ; i++ ))
   do
      eval "exec $i<&-"
   done
}
export -f closefds

exec 99>&1
( echo "closefds" ;  prefixitin ' in: ' ) | bash 1> >(prefixitout 'out: ') 2> >(prefixiterr 'ERR: ')
EOF3

chmod +x /home/knoppix/simple-guest-bash-server.sh

mkdir -p /home/knoppix/.config/lxsession/LXDE
cat >/home/knoppix/.config/lxsession/LXDE/autostart <<'EOF4'
bash -c "sleep 5 ; xterm -e bash -c 'top;bash' & xterm -e bash -c 'pkill screensaver ; export xtermtty=\$(tty) ; /from-knoppix704/busybox nc -ll -p 11222 -e /home/knoppix/simple-guest-bash-server.sh ; bash'"
EOF4

## extra lines for minirt/init end
EOF

    # THE REST
    tail -n $(( totLines - execLine + 2 )) init.org >>init

    diff init.org init || true
    
    # copy out for debugging
    cp init init.org ../

    # copy in busybox from knoppix 7.0.4 DVD
    mkdir from-knoppix704
    cp "$bbpath"/busybox "$bbpath"/busybox.sha1 from-knoppix704
)


# (4) repack minirt.gz

(
    cd "$targetdir"
    mv minirt.gz minirt.gz.org

    cd expanded
    find . | cpio -oH newc | gzip -9 > ../minirt.gz

    ## go ahead and get rid of expanded/
    cd ..
    rm -fr expanded
)

# (5) make soft link to iso file

ln -s "$kiso" "$targetdir/link-to-knoppix.iso"
echo "${kiso##*/}" >"$targetdir/iso-filename.txt"

# (6) output startup script

# original line from syslinux.cfg in KNOPPIX_V7.0.4DVD-2012-08-20-EN.iso
kcmdline="ramdisk_size=100000 lang=en apm=power-off initrd=minirt.gz nomce libata.force=noncq hpsa.hpsa_allow_any=1 loglevel=1 tz=localtime"

cat >"$targetdir/start-scriptable-kvm.sh" <<EOF
reportfail()
{
    echo "Failed (\$*). Exiting." 1>&2
    exit 255
}
export SCRIPT_DIR="\$(cd "\$(dirname "\$(readlink -f "\$0")")" && pwd -P)" || reportfail  # use -P to get expanded absolute path

# All options are set with environment variables.  Set to " " (one space) to disable.

# (1) Knoppix ISO file
theiso=""
orgloc="\$(readlink -f "\$SCRIPT_DIR/link-to-knoppix.iso")" # link gets preference
[ -f "\$orgloc" ] && theiso="\$orgloc"
[ "\$theiso" != "" ] || theiso="\$KVMISO" # set by dinkvm script
[ "\$theiso" != "" ] || reportfail "could not locate knoppix iso file"

# (2) forwarded ports
[ "\$bashHPORT" = "" ] && bashHPORT="10199"
[ "\$sshHPORT" = "" ] && sshHPORT="10122"
[ "\$httpHPORT" = "" ] && httpHPORT="10180"
[ "\$monHPORT" = "" ] && monHPORT="10197"

portforward="hostfwd=tcp:127.0.0.1:\$bashHPORT-:11222"
portforward="hostfwd=tcp:127.0.0.1:\$sshHPORT-:22,\$portforward"
portforward="hostfwd=tcp:127.0.0.1:\$httpHPORT-:80,\$portforward"

# (3) user mode network
[ "\$usernet" = "" ] && usernet="-net nic,vlan=0,model=virtio -net user,vlan=0,\$portforward"

# (4) mcast network
[ "\$mcastPORT" = "" ] && mcastPORT="1234"
[ "\$mcastMAC" = "" ] && mcastMAC="52:54:00:12:00:00"
[ "\$mcastnet" = "" ] && mcastnet="-net nic,vlan=1,macaddr=\$mcastMAC  -net socket,vlan=1,mcast=230.0.0.1:1234"

# (5) memory
[ "\$kvmMEM" = "" ] && kvmMEM="-m 1024"

# (6) kvm display
[ "\$kvmDISP" = "" ] && kvmDISP=" " # use default (-sdl)

# (7) kvm vga card
[ "\$kvmVGA" = "" ] && kvmVGA="-vga vmware"

# (8) kvmMISC
[ "\$kvmMISC" = "" ] && kvmMISC=" "

# (1-8) put it together
[ "\$kvmparams" = "" ] && kvmparams="-cdrom \$theiso \$usernet \$mcastnet \$kvmMEM \$kvmDISP \$kvmVGA \$kvmMISC"

[ "\$KVMBIN" = "" ] && KVMBIN="kvm"
[ "\$KVMKERNEL" = "" ] && KVMKERNEL="./linux"

# (9) linux kernel command line
kcmdline="$kcmdline"
# remove old minirt.gz parameter, because it will be loaded directly by KVM
kcmdline="\${kcmdline/initrd=minirt.gz /}"
kcmdline="\${kcmdline} \$kcmdlineMISC" # add extras from calling script

cd "\$SCRIPT_DIR"
# the eval is used because the quotes in $kvmparams need to be parsed
eval thewholething=( \$KVMBIN \$kvmparams -monitor telnet::\$monHPORT,server,nowait -kernel \$KVMKERNEL -initrd "./minirt.gz" )
# but don't eval here so kcmdline does not go through word splitting
thewholething=( "\${thewholething[@]}" -append "\$kcmdline" )

# make first line of kvm.out be the something that can be evaled to recreate thewholething array
declare -p thewholething

exec "\${thewholething[@]}"
EOF
chmod +x "$targetdir/start-scriptable-kvm.sh"