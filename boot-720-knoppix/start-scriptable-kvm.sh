reportfail()
{
    echo "Failed ($*). Exiting." 1>&2
    exit 255
}
export SCRIPT_DIR="$(cd "$(dirname "$(readlink -f "$0")")" && pwd -P)" || reportfail  # use -P to get expanded absolute path

# All options are set with environment variables.  Set to " " (one space) to disable.

# (1) Knoppix ISO file
theiso=""
orgloc="$(readlink -f "$SCRIPT_DIR/link-to-knoppix.iso")" # link gets preference
[ -f "$orgloc" ] && theiso="$orgloc"
[ "$theiso" != "" ] || theiso="$KVMISO" # set by dinkvm script
[ "$theiso" != "" ] || reportfail "could not locate knoppix iso file"

# (2) forwarded ports
[ "$bashHPORT" = "" ] && bashHPORT="10199"
[ "$sshHPORT" = "" ] && sshHPORT="10122"
[ "$httpHPORT" = "" ] && httpHPORT="10180"
[ "$monHPORT" = "" ] && monHPORT="10197"

portforward="hostfwd=tcp:127.0.0.1:$bashHPORT-:11222"
portforward="hostfwd=tcp:127.0.0.1:$sshHPORT-:22,$portforward"
portforward="hostfwd=tcp:127.0.0.1:$httpHPORT-:80,$portforward"

# (3) user mode network
[ "$usernet" = "" ] && usernet="-net nic,vlan=0,model=virtio -net user,vlan=0,$portforward"

# (4) mcast network
[ "$mcastPORT" = "" ] && mcastPORT="1234"
[ "$mcastMAC" = "" ] && mcastMAC="52:54:00:12:00:00"
[ "$mcastnet" = "" ] && mcastnet="-net nic,vlan=1,macaddr=$mcastMAC  -net socket,vlan=1,mcast=230.0.0.1:1234"

# (5) memory
[ "$kvmMEM" = "" ] && kvmMEM="-m 1024"

# (6) kvm display
[ "$kvmDISP" = "" ] && kvmDISP=" " # use default (-sdl)

# (7) kvm vga card
[ "$kvmVGA" = "" ] && kvmVGA="-vga vmware"

# (8) kvmMISC
[ "$kvmMISC" = "" ] && kvmMISC=" "

# (1-8) put it together
[ "$kvmparams" = "" ] && kvmparams="-cdrom $theiso $usernet $mcastnet $kvmMEM $kvmDISP $kvmVGA $kvmMISC"

[ "$KVMBIN" = "" ] && KVMBIN="kvm"
[ "$KVMKERNEL" = "" ] && KVMKERNEL="./linux"

# (9) linux kernel command line
kcmdline="ramdisk_size=100000 lang=en apm=power-off initrd=minirt.gz nomce libata.force=noncq hpsa.hpsa_allow_any=1 loglevel=1 tz=localtime"
# remove old minirt.gz parameter, because it will be loaded directly by KVM
kcmdline="${kcmdline/initrd=minirt.gz /}"
kcmdline="${kcmdline} $kcmdlineMISC" # add extras from calling script

cd "$SCRIPT_DIR"
# the eval is used because the quotes in  need to be parsed
eval thewholething=( $KVMBIN $kvmparams -monitor telnet::$monHPORT,server,nowait -kernel $KVMKERNEL -initrd "./minirt.gz" )
# but don't eval here so kcmdline does not go through word splitting
thewholething=( "${thewholething[@]}" -append "$kcmdline" )

# make first line of kvm.out be the something that can be evaled to recreate thewholething array
declare -p thewholething

exec "${thewholething[@]}"
